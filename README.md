# The Franz Ferdinand Linrary

The Franz Ferdinand Linrary is an open source C++ header only library. 
It is meant 

* as a convenience layer on top of STL / Boost. 
* a foundation layer for algorithmic trading. It doesn't give awayany IP. But if used by other groups makes integration easier.

The sub libraries:

* **ferdi/num** inspired by [numpy](http://www.numpy.org/) funcionality with a focus on simple interfaces (eg. AnnualizedSharpeRatio(), MaxDrawdown())
* **ferdi/trading** the foundations of an algo trading library (enums, price, tick size, currencies, ....)
* **ferdi/python** utilities ont he Boost-Python interface like a converter of a Py-dictionary to a Boost-PTree
* **ferdi/iostreams** eg. ZipFile(), interface inspired by Linux tool gzip
* CMakeLists.txt is just needed for unit tests

Clearly, the library is in it's infancy and doesn't have the critical mass yet. But this is meant to change and you are welcome to contribute.
