#include <gtest/gtest.h>
#include <ferdi/string/string_conversions_hex.h>

namespace ferdi {
namespace tec {
namespace ut {
    
TEST(Tec, StringConversionsHex) {
    std::vector<std::string> test_cases = { "", "Z", "aZ", "aZa", "aaaa5aa", "()*#?",
                                            "zxcvbnwertyuiop1234567890", "_-_-"
                                            "uUGNkfZPSufWZa0N1c2zXor" };
    for (const auto& s: test_cases) {
        const auto hex = ferdi::tec::StringToHex(s);
        ASSERT_EQ(s, ferdi::tec::HexToString(hex));
    }    
}    
   
}
}
}
