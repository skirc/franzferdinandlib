#include <gtest/gtest.h>
#include <ferdi/string/integer_conversions.h>

namespace ferdi {
namespace tec {
namespace ut {
    
TEST(Tec, IntegerConversions) {
    std::vector<std::string> test_cases = { "0", "100", "123", "12345678", "1234567890" };
    for (const auto& s: test_cases) {
        ASSERT_EQ(std::stoi(s), ferdi::tec::ArrayToPositiveInteger(s.c_str()));
    }    
}    
   
}
}
}