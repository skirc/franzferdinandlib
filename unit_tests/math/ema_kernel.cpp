#include <gtest/gtest.h>
#include <ferdi/math/ema_kernel.h>

namespace ferdi {
namespace math {
namespace ut {

////////////////////////////////////////////////////////////////////////////////    
TEST(Math, EmaKernel) {
    const double zero(0);
    const double one(1);
    const double two(2);
    EmaKernel<true, double> ema_init_zero(0.5);
    EmaKernel<false, double> ema_init_nan(0.5);
    
    // ------ Initial Value
    ASSERT_EQ(ema_init_zero.get_ema(), zero);
    ASSERT_NE(ema_init_nan.get_ema(), zero);
    
    // ------ First Value
    ema_init_zero.Update(two);
    ema_init_nan.Update(two);
    ASSERT_LT(ema_init_zero.get_ema(), two);
    ASSERT_EQ(ema_init_nan.get_ema(), two);
    ASSERT_EQ(ema_init_zero.get_nbr_updates(), 1);
    ASSERT_EQ(ema_init_nan.get_nbr_updates(), 1);
    
    // ------ Other trivial checks
    ema_init_nan.Update(two);
    ASSERT_EQ(ema_init_nan.get_ema(), two);
    
    // ------ Inifinit Memory
    EmaKernel<false, double> ema_inf_memory(zero);
    ASSERT_NE(ema_inf_memory.get_ema(), zero);
    ema_inf_memory.Update(two);
    ASSERT_EQ(ema_inf_memory.get_ema(), two);
    ema_inf_memory.Update(one);
    ASSERT_EQ(ema_inf_memory.get_ema(), two);
    ASSERT_EQ(ema_inf_memory.get_nbr_updates(), 2); 
    
    // ------ No Memory
    std::vector<double> test_cases = { two, one, zero };
    EmaKernel<false, double> ema_no_memory(one);
    ASSERT_EQ(ema_no_memory.get_nbr_updates(), 0);
    for (const auto tc: test_cases) {
        ema_no_memory.Update(tc);
        ASSERT_EQ(ema_no_memory.get_ema(), tc);
    }
    
    // ------  Conversion alpha ---> Periods
    ASSERT_EQ(two, GetEmaPeriodsFromAlpha(GetEmaAlphaFromPeriods(two)));
}

}
}
}