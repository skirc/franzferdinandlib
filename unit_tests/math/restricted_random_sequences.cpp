#include <gtest/gtest.h>
#include <ferdi/math/restricted_random_sequences.h>

namespace ferdi {
namespace math {
namespace ut {

////////////////////////////////////////////////////////////////////////////////    
TEST(Math, RestrictedRandomSequences) {
    const double floor = 1.0;
    const double sum = 100.0;
    UniformFloorAndSumRestrictedRandomSequences<double> generator(42, floor, sum);
    
    // ----- length 1
    const auto seq_length_1 = generator.GenerateSequence(1);
    ASSERT_EQ(seq_length_1.size(), 1);
    ASSERT_EQ(seq_length_1.back(), sum);
    
    // ----- length > 1
    std::cout << std::fixed << std::setprecision(10);
    for (size_t i=2; i<100; i++) {
        const auto seq_length = generator.GenerateSequence(i);
        //std::cout << "GTest   " << i << "   Sum " << std::accumulate(seq_length.begin(), seq_length.end(), 0.0) << "  Min " << *std::min_element(seq_length.begin(), seq_length.end()) << '\n';
        ASSERT_EQ(i, seq_length.size());
        ASSERT_LT(fabs(sum - std::accumulate(seq_length.begin(), seq_length.end(), 0.0)), 0.0000001);
        ASSERT_LE(floor, *std::min_element(seq_length.begin(), seq_length.end()));
    }
    
    
}

}
}
}