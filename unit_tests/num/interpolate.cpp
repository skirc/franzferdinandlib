// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#include <gtest/gtest.h>
#include <ferdi/num/interpolate.h>
#include <math.h>

using namespace std;

namespace ferdi {
namespace num {
namespace ut {

////////////////////////////////////////////////////////////////////////////////
TEST(Num, Interpolate) {
    const vector<double> values_x( {2, 4, 6 } );
    const vector<double> values_y( {2, 4, 6 } );
    const bool extrapolate_const = false;
    const bool extrapolate_linear = true;

    const double interpol_value = 3.0;
    const double front_extraplo_value = 0.0;

    // --- not enough points
    ASSERT_TRUE(isnan(Interpolate({},  {},  interpol_value, extrapolate_const)));
    ASSERT_TRUE(isnan(Interpolate({1}, {2}, interpol_value, extrapolate_const)));

    // --- interpolation
    ASSERT_EQ(Interpolate(values_x, values_y, interpol_value, extrapolate_const), interpol_value);
    ASSERT_EQ(Interpolate(values_x, values_y, values_y.front(), extrapolate_const), values_y.front());
    ASSERT_EQ(Interpolate(values_x, values_y, values_y.back(),  extrapolate_const), values_y.back());

    // --- extrapolation
    ASSERT_EQ(Interpolate(values_x, values_y, front_extraplo_value, extrapolate_const),  values_y.front());
    ASSERT_EQ(Interpolate(values_x, values_y, front_extraplo_value, extrapolate_linear), 0.0);
}

} // namespace
} // namespace num
} // namespace ferdi
