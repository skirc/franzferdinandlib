// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#include <gtest/gtest.h>
#include <ferdi/num/arithmetic_operations.h>

using namespace std;

namespace ferdi {
namespace num {
namespace ut {
    
TEST(Num, ArithmeticOperations) {
    const vector<double> ones = { 1, 1, 1, 1, 1 };
    const vector<double> twos = { 2, 2, 2, 2, 2 };
    ASSERT_EQ(ferdi::num::Add(ones, ones), twos);
}    
  
} // namespace 
} // namespace 
} // namespace