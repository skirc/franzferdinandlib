// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#include <list>
#include <gtest/gtest.h>
#include <ferdi/num/algorithm.h>
#include <ferdi/num/correlation_coef.h>
#include <ferdi/num/covariance_coef.h>
#include <ferdi/trading/price.h>

using namespace std;

namespace ferdi {
namespace num {
namespace ut {
    
TEST(Num, CorrCoef) {
    
    // ---- test vector
    const vector<double> tc1_x = { -1, 4, -2, 4, -3, -1,  5 };
    const vector<double> tc1_y = { -2, 5,  3, 4, -2, -6, 12 };
    const vector<double> zero(tc1_x.size(), 0.0);
    const vector<double> empty;
    const vector<double> tc2_x(1, 3.0);
    const vector<double> tc2_y(1, 2.0);
    const double tc1_expt = 0.7764335147641444612177;   // taken from numpy.corrcoef()
    ASSERT_EQ(CorrCoef(tc1_x, tc1_x), 1.0);
    ASSERT_EQ(CorrCoef(tc1_x, tc1_y), tc1_expt);
    ASSERT_EQ(CorrCoef(tc1_y, tc1_x), tc1_expt);
    EXPECT_TRUE(isnan(CorrCoef(tc1_x, zero))); 
    EXPECT_TRUE(isnan(CorrCoef(tc1_x, empty)));
    EXPECT_TRUE(isnan(CorrCoef(empty, empty)));
    EXPECT_TRUE(isnan(CorrCoef(tc2_x, tc2_y)));
    
    // ---- test list
    const list<double> tc1_list_x(tc1_x.begin(), tc1_x.end());
    const list<double> tc1_list_y(tc1_y.begin(), tc1_y.end());
    ASSERT_EQ(CorrCoef(tc1_list_x, tc1_list_y), tc1_expt);
    
    // ---- test Covariance
    const double tc1_cov_expt = 15.333333333333333333333;  // taken from numpy.cov()
    ASSERT_EQ(CovCoef(tc1_x, tc1_y), tc1_cov_expt);
    ASSERT_EQ(CovCoef(tc1_x, zero), 0.0);
    
    // ---- test vector for Price
    const vector<trd::Price> price_x(tc1_x.begin(), tc1_x.end());
    const vector<trd::Price> price_y(tc1_y.begin(), tc1_y.end());
    ASSERT_EQ(CovCoef(price_x, price_y).get_value(), tc1_cov_expt);
}    

}
}
}
