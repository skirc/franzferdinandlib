#include <gtest/gtest.h>
#include <ferdi/trading/trading_enums_string.h>
#include <ferdi/trading/price.h>
#include <ferdi/trading/exec_side_compile_time_traits.h>
#include <ferdi/trading/exec_side_run_time_traits.h>

//using namespace ferdi::trd;

namespace ferdi {
namespace trd {
namespace ut {

////////////////////////////////////////////////////////////////////////////////    
TEST(Trading, EnumStrings) {
    const ExecSide buy  = ExecSide::BUY;
    const ExecSide sell = ExecSide::SELL;
    const LiquidityType maker = LiquidityType::MAKER;
    const LiquidityType taker = LiquidityType::TAKER;
    
    // ------------- Exec Side
    ASSERT_EQ(buy,  ExecSideFromString(ToString(buy)));
    ASSERT_EQ(buy,  ExecSideFromString(ToProperCaseString(buy)));
    ASSERT_EQ(buy,  ExecSideFromString("BUY"));
    ASSERT_EQ(buy,  ExecSideFromString("Buy"));
    ASSERT_EQ(sell, ExecSideFromString(ToString(sell)));
    ASSERT_EQ(sell, ExecSideFromString(ToProperCaseString(sell)));
    ASSERT_EQ(buy,  ExecSideFromString(std::string("BUY").c_str()));
    
    ASSERT_EQ(buy,  ExecSideFromChar(static_cast<char>(buy)));
    ASSERT_EQ(sell, ExecSideFromChar(static_cast<char>(sell)));
    
    // ------------- Liquidity Type
    ASSERT_EQ(maker,  LiquidityTypeFromChar(static_cast<char>(maker)));
    ASSERT_EQ(taker,  LiquidityTypeFromChar(static_cast<char>(taker)));
    ASSERT_EQ(maker,  LiquidityTypeFromString(ToProperCaseString(maker)));
    ASSERT_EQ(taker,  LiquidityTypeFromString(ToProperCaseString(taker)));
    
}    
   
}
}
}
