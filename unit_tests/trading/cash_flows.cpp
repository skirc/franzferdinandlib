// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#include <gtest/gtest.h>
#include <ferdi/trading/cash_flows.h>
#include <ferdi/trading/price.h>

using namespace std;

namespace ferdi {
namespace trd {
namespace ut {
    
TEST(Trading, CashFlows) {
    // ------- Definitions
    const auto usd = Currency::USD;
    const auto eur = Currency::EUR;
    const auto sgd = Currency::SGD;
    const auto chf = Currency::CHF;
    const CashFlow<Price> cf_usd(Price(1), usd);
    const CashFlow<Price> cf_eur(Price(1), eur);
    const CashFlow<Price> cf_sgd(Price(12), sgd);
    
    // ------- Tests
    const CashFlows<Price> cfs_usd(cf_usd);
    ASSERT_EQ(cfs_usd, cf_usd);
    auto cfs_eur_usd = cf_usd + cf_eur;
    ASSERT_EQ(cfs_eur_usd.size(), 2);
    ASSERT_FALSE(cfs_eur_usd == cf_usd);
    cfs_eur_usd = cfs_eur_usd + cf_eur;
    ASSERT_EQ(cfs_eur_usd.size(), 2);
    auto cfs_eur_usd_sgd = cfs_eur_usd + cf_sgd;
    ASSERT_EQ(cfs_eur_usd_sgd.size(), 3);
    ASSERT_EQ(cfs_eur_usd_sgd.GetCurrencyAmount(sgd), cf_sgd.amount);
    ASSERT_EQ(cfs_eur_usd_sgd.GetCurrencyAmount(chf), Price(0));
    ASSERT_EQ(cfs_eur_usd, cfs_eur_usd);
    ASSERT_FALSE(cfs_eur_usd == cfs_eur_usd_sgd);
    ASSERT_TRUE(cfs_eur_usd_sgd.HasCurrency(usd));
    ASSERT_TRUE(cfs_eur_usd_sgd.HasCurrency(sgd));
    ASSERT_FALSE(cfs_eur_usd_sgd.HasCurrency(chf));
}    
   
}
}
}