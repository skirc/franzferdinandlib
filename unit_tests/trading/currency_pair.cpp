// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#include <gtest/gtest.h>
#include <ferdi/trading/currency_pair.h>
#include <ferdi/trading/currency_string.h>

using namespace std;

namespace ferdi {
namespace trd {
namespace ut {
    
TEST(Trading, CurrencyPair) {
    const auto usd = Currency::USD;
    const auto eur = Currency::EUR;
    const auto sgd = Currency::SGD;
    const auto chf = Currency::CHF;
    const CurrencyPair eurusd(eur, usd);
    const CurrencyPair eurxbt(eur, Currency::XBT);
    const CurrencyPair invalid;
    ASSERT_EQ(eurusd, eurusd);
    ASSERT_EQ(eurusd, eurusd.GetInverted().GetInverted());
    ASSERT_NE(eurusd, eurusd.GetInverted());
    ASSERT_TRUE(eurusd.IsValid());
    ASSERT_FALSE(invalid.IsValid());
    ASSERT_FALSE(CurrencyPair(eur, Currency::UNKNOWN).IsValid());
    ASSERT_FALSE(eurusd < eurusd);
    ASSERT_TRUE(eurusd == eurusd);
    ASSERT_TRUE(eurusd != eurxbt);
    ASSERT_TRUE(eurusd.Contains(eur));
    ASSERT_FALSE(eurusd.Contains(chf));
    ASSERT_EQ(usd, eurusd.GetOther(eur));
    
    
    // Tests for CurrencyPairFromString
    const vector<std::pair<Currency, Currency>> pairs = { make_pair(eur, usd), 
                                                          make_pair(chf, sgd), 
                                                          make_pair(chf, chf) };
    for (const auto& p: pairs) {
        const string sp = ToString(p.first) + ToString(p.second);
        const CurrencyPair ep(p.first, p.second);
        ASSERT_EQ(CurrencyPairFromString(sp), ep);
        ASSERT_EQ(sp, ToString(ep));
    }
    
    ASSERT_EQ(CurrencyPairFromString("IDR").base_ccy, Currency::UNKNOWN);
    ASSERT_EQ(CurrencyPairFromString("IDRUSDD").base_ccy, Currency::UNKNOWN);
    
    ASSERT_FALSE(eurusd.ContainsCrypto());
    ASSERT_TRUE(eurxbt.ContainsCrypto());
}    
   
}
}
}