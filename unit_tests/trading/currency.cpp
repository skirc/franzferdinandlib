// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#include <gtest/gtest.h>
#include <ferdi/trading/currency.h>
#include <ferdi/trading/currency_string.h>

using namespace std;

namespace ferdi {
namespace trd {
namespace ut {
    
TEST(Trading, Currency) {
    vector<string> currencies = { "EUR", "USD", "SGD", "HKD", 
                                  "KRW", "THB", "MYR", "PHP", "TWD", "IDR", "BND", "KHR",
                                  "IDR", "CZK", "RUB", "BRL",
                                  "ARS", "CNY" };
    for (const auto& ccy: currencies) {
        ASSERT_EQ(ccy, ToString(CurrencyFromString(ccy)));
    }
}    
   
}
}
}
