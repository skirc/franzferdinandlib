#include <gtest/gtest.h>
#include <ferdi/trading/trading_enums_string.h>
#include <ferdi/trading/price.h>
#include <ferdi/trading/exec_side_compile_time_traits.h>
#include <ferdi/trading/exec_side_run_time_traits.h>

//using namespace ferdi::trd;

namespace ferdi {
namespace trd {
namespace ut {


////////////////////////////////////////////////////////////////////////////////    
TEST(Trading, ExecSideTraits) {
    const ExecSide buy  = ExecSide::BUY;
    const ExecSide sell = ExecSide::SELL;
    const double d42 = 42.0;
        
    // ----- OtherSide
    ASSERT_EQ(buy,  OtherSide(sell));
    ASSERT_EQ(buy,  OtherSide<sell>());
    ASSERT_EQ(sell, OtherSide(buy));
    ASSERT_EQ(sell, OtherSide<buy>());
    ASSERT_NE(buy, OtherSide(buy));
    
    // ----- IsGreater
    ASSERT_TRUE (IsGreaterThan(sell,  2,  1));
    ASSERT_TRUE (IsGreaterThan(sell,  2, -1));
    ASSERT_TRUE (IsGreaterThan(sell, -1, -2));
    ASSERT_TRUE (IsGreaterThan(buy,   1,  2));
    ASSERT_TRUE (IsGreaterThan(buy,  -2, -1));
    ASSERT_FALSE(IsGreaterThan(sell,  1,  2));
    ASSERT_FALSE(IsGreaterThan(buy,   2,  1));
    ASSERT_TRUE (IsGreaterThan(sell,  0, -1));   // test positive
    ASSERT_TRUE (IsGreaterThan(buy,   0,  1));
    
    // ----- IsGreaterInvertRightSide
    ASSERT_TRUE (IsGreaterInvertRightSide(sell,  3,  1));
    ASSERT_TRUE (IsGreaterInvertRightSide(sell,  3, -1));
    ASSERT_TRUE (IsGreaterInvertRightSide(sell, -1, -3));
    ASSERT_FALSE(IsGreaterInvertRightSide(sell,  1,  3));
    ASSERT_FALSE(IsGreaterInvertRightSide(buy,   1,  3));    
    ASSERT_FALSE(IsGreaterInvertRightSide(buy,   1,  0));
    ASSERT_TRUE (IsGreaterInvertRightSide(buy,  -4,  3));    
    ASSERT_TRUE (IsGreaterInvertRightSide(buy,   1, -2));
    
    // ----- IsLessInvertRightSide
    ASSERT_TRUE (IsLessInvertRightSide(sell,   1, 2));
    ASSERT_TRUE (IsLessInvertRightSide(buy,    1, 2));
    
    // ----- PositionAddQty
    ASSERT_EQ(7.0,   PositionAddQty(buy, 5.0, 2.0));
    ASSERT_EQ(2.0,   PositionAddQty(buy, -5.0, 7.0));
    ASSERT_EQ(3.0,   PositionAddQty(sell, 5.0, 2.0));
    ASSERT_EQ(-6.0,  PositionAddQty(sell, -5.0, 1.0));
    
    // ----- IsNegative
    ASSERT_FALSE (IsNegative(sell,  d42));    
    
    // ----- Mixed Run Time
    ASSERT_EQ(IsGreaterThan(sell,  0.0,  d42), IsNegative(sell, d42));
    ASSERT_EQ(IsGreaterThan(buy,   0.0,  d42), IsNegative(buy,  d42));
    ASSERT_EQ(IsGreaterThan(buy,   0.0,  d42), IsPositive(buy, -d42));
    
    // ----- Run Time vs Compile Time 
    std::vector<std::pair<double, double>> test_cases = { std::make_pair(2, 7), std::make_pair(2, -7) };
    for (const auto& tc: test_cases) {
        ASSERT_EQ(Diff<buy>(tc.first, tc.second), Diff(buy, tc.first, tc.second));
        ASSERT_EQ(Diff<sell>(tc.first, tc.second), Diff(sell, tc.first, tc.second));
        ASSERT_EQ(Add<buy>(Price(tc.first), ferdi::trd::Price(tc.second)), ferdi::trd::Price(Add(buy, tc.first, tc.second)));
    }
}    
   
}
}
}
