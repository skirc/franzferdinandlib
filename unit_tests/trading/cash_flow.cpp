// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#include <gtest/gtest.h>
#include <ferdi/trading/cash_flows.h>
#include <ferdi/trading/price.h>

using namespace std;

namespace ferdi {
namespace trd {
namespace ut {
    
struct DoubleWrapper {
    friend std::ostream& operator<<(std::ostream& os, const DoubleWrapper& a) { os << (*a.amount); return os; }
    DoubleWrapper(double* a) : amount(a) {}
    bool operator==(const DoubleWrapper& rhs) const { return *amount == *(rhs.amount); }
    bool operator!=(const DoubleWrapper& rhs) const { return *amount != *(rhs.amount); }
    double* amount = nullptr;
};    
    
TEST(Trading, CashFlow) {
    typedef CashFlow<Price>   PriceCashFlow;
    typedef CashFlow<double>  DoubleCashFlow;
    typedef CashFlow<DoubleWrapper>  DoublePtrCashFlow;
    
    const double one = 1.0;
    const auto usd = Currency::USD;
    const auto eur = Currency::EUR;
    const auto sgd = Currency::SGD;
    
    // --------------- Price
    const PriceCashFlow cf_price_usd(Price(one), usd);
    const PriceCashFlow cf_price_eur(Price(one), eur);
    ASSERT_EQ(cf_price_usd, cf_price_usd);
    ASSERT_NE(cf_price_usd, cf_price_eur);
    
    // --------------- Double
    const DoubleCashFlow cf_double_usd(one, usd);
    const DoubleCashFlow cf_double_eur(one, eur);
    const DoubleCashFlow cf_double_sgd_6(6.0, sgd);
    const DoubleCashFlow cf_double_sgd_12(12.0, sgd);
    ASSERT_EQ(cf_double_usd, cf_double_usd);
    ASSERT_NE(cf_double_usd, cf_double_eur);
    ASSERT_EQ(cf_double_usd * one, cf_double_usd);
    ASSERT_EQ(cf_double_sgd_6 * 2.0, cf_double_sgd_12);
    
    // --------------- Double Ptr
    double two = 2;
    const DoublePtrCashFlow cf_ptr_usd(&two, usd);
    const DoublePtrCashFlow cf_ptr_eur(&two, eur);
    ASSERT_EQ(cf_ptr_usd, cf_ptr_usd);
    ASSERT_NE(cf_ptr_usd, cf_ptr_eur);
}    
   
}
}
}