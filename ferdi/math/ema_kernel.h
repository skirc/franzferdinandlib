// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <stddef.h>
#include <numeric>
#include <limits>

namespace ferdi {
namespace math {
    
/**************************************************************************//**
    * Calculates the Exponential Moving Average and maintains the internal state between calls.
    *   - First template parameter defines initial condition
    *   - alpha = 1 ---> no memory  
    * Usage:
    *   EmaKernel<true, double> ema_kernel(0.5); 
    *   ema_kernel.Update(2.0);
    *   auto current_ema = ema_kernel.get_ema();  */
template<bool ZeroInit, typename ValueType>    
class EmaKernel {
public:
    EmaKernel(ValueType alpha) : alpha_(alpha), one_minus_alpha_(1.0 - alpha) { Reset(); }
    void Reset();
    void Update(ValueType value);
    ValueType get_alpha()           const { return alpha_; }
    ValueType get_one_minus_alpha() const { return one_minus_alpha_; }
    ValueType get_ema()             const { return ema_; }
    size_t    get_nbr_updates()     const { return nbr_updates_; }
    
private:
    ValueType EmaUpdateEquation(ValueType value) const { return alpha_ * value + one_minus_alpha_ * ema_; }
    
private:
    ValueType       ema_ = 0;
    size_t          nbr_updates_ = 0;
    const ValueType alpha_ = 0.5;
    const ValueType one_minus_alpha_ = 0.5;
};

////////////////////////////////////////////////////////////////////////////////
// Convenience methods to convert between alpha and nbr of periods
double GetEmaAlphaFromPeriods(double N);
double GetEmaPeriodsFromAlpha(double alpha);

////////////////////////////////////////////////////////////////////////////////
//////////////////////// inline implementations ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
template<> 
inline void EmaKernel<true, double>::Update(double value) {
    ema_ = EmaUpdateEquation(value);
    nbr_updates_++;
}

template<> 
inline void EmaKernel<false, double>::Update(double value) {
    if (nbr_updates_ > 0) {
        ema_ = EmaUpdateEquation(value);
    } else {
        ema_ = value; 
    }    
    nbr_updates_++;
}

template<bool ZeroInit, typename ValueType> 
inline void EmaKernel<ZeroInit, ValueType>::Reset() { 
    nbr_updates_ = 0;
    if (ZeroInit) {
        ema_ = 0.0;
    } else {
        ema_ = std::numeric_limits<ValueType>::quiet_NaN();
    }
}

/** https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average */
inline double GetEmaAlphaFromPeriods(double N) { 
    return 2.0 / (1.0 + N); 
}

/** Inverse operation to GetEmaAlphaFromPeriods() */
inline double GetEmaPeriodsFromAlpha(double alpha) {
    return (2.0 / alpha) - 1.0;
}

} // namespace math
} // namespace ferdi

