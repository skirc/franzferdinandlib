// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <assert.h> 
#include <random>
#include <vector>
#include <algorithm>
#include <iostream>

namespace ferdi {
namespace math {
    
/**************************************************************************//**
    *  Generates a sequence of random numbers of length N with the extra conditions:
    *    - sum is constraint to a constant value
    *    - all random numbers >= floor
    *  Type must be a RealType */    
template<class Type> 
class UniformFloorAndSumRestrictedRandomSequences {
    typedef std::uniform_real_distribution<Type>  DistributionType;
    typedef std::vector<Type>                     SequenceType;
public:
    UniformFloorAndSumRestrictedRandomSequences(unsigned int seed, Type floor, Type sum);
    SequenceType GenerateSequence(size_t length);
    std::mt19937& get_random_generator() { return random_generator_; } //generator for reuse elsewhere
private:    
    std::mt19937        random_generator_;
    DistributionType    uniform_distribution_;
    const Type          sum_;
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
template<class Type> 
inline UniformFloorAndSumRestrictedRandomSequences<Type>::UniformFloorAndSumRestrictedRandomSequences(unsigned int seed, Type floor, Type sum)
    : random_generator_(seed)
    , uniform_distribution_(floor, sum - floor)
    , sum_(sum) {    
}

template<class Type> 
inline typename UniformFloorAndSumRestrictedRandomSequences<Type>::SequenceType UniformFloorAndSumRestrictedRandomSequences<Type>::GenerateSequence(size_t length) {
    assert(length * floor_ < sum_);
    SequenceType sequence;
    sequence.reserve(length);
    if (length > 1) {
        Type current_sum = 0;
        Type current_min = std::numeric_limits<Type>::max();
        for (size_t i=0; i<length; i++) {
            const auto rn = uniform_distribution_(random_generator_);
            current_sum += rn;
            current_min = std::min(current_min, rn);
            sequence.emplace_back(rn);
        }
        const auto sum_scale_factor = sum_ / current_sum;
        const auto sum_scaled_min = current_min * sum_scale_factor;
        if (sum_scaled_min >= uniform_distribution_.min()) {
            std::transform(sequence.begin(), sequence.end(), sequence.begin(), [sum_scale_factor](auto& c){return c * sum_scale_factor ; });
        } else {
            const auto offset_sum_scale_factor = (sum_ - length * uniform_distribution_.min())  / (current_sum - length * current_min);
            if (offset_sum_scale_factor > 0.0) {
                const auto floor = uniform_distribution_.min();
                std::transform(sequence.begin(), sequence.end(), sequence.begin(), [floor, current_min, offset_sum_scale_factor](auto& c) { 
                    return (c - current_min) * offset_sum_scale_factor + floor; 
                });
            } else {
                // This should never happen since it should be caught by the assert
                std::cout << __FUNCTION__ << " Ill formed request for length " << length << " and floor " << uniform_distribution_.min() << '\n';
                sequence.clear();
            }
        }
        
    } else if (length == 1) {
        sequence.emplace_back(sum_);
    }
    return sequence;
}

} // namespace math
} // namespace ferdi

