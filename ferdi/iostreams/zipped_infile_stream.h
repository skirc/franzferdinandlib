// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <memory>
#include <fstream>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/gzip.hpp>

namespace ferdi {
namespace iostreams {

/***************************************************************************//**
   * Wrapper around streaming in a zipped file. 
   *  'filename' has to be a '*.gz' file.
   *  Usage (almost like std::istream):
   *     ZippedInfileStream file(filename);
   *     if (file) {
   *         file.get_unzipped_istream() >> dummy_value; 
   *     }                                                                    */
class ZippedInfileStream {
    typedef boost::iostreams::filtering_streambuf<boost::iostreams::input> StreamBuffer;
    typedef std::unique_ptr<std::istream> IStreamPtr;
public:    
    ZippedInfileStream(const std::string& filename);
    explicit operator bool() const { return unzipped_stream_.get() != nullptr; }
    operator std::istream&() { return *unzipped_stream_; }
    std::istream& get_unzipped_istream() const { return *unzipped_stream_; }
    
private:   
    std::ifstream  zipped_file_;
    StreamBuffer   in_buffer_;
    IStreamPtr     unzipped_stream_;
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline ZippedInfileStream::ZippedInfileStream(const std::string& filename) 
    : zipped_file_(filename) {
    if (zipped_file_) {
        in_buffer_.push(boost::iostreams::gzip_decompressor());
        in_buffer_.push(zipped_file_);         
        unzipped_stream_ = std::make_unique<std::istream>(&in_buffer_);
    }
}

}  // namespace iostreams
}  // namespace ferdi