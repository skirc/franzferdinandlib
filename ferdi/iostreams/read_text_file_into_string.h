// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#pragma once

#include <memory>
#include <string>
#include <fstream>
#include <sstream>
#include <boost/filesystem.hpp>

namespace ferdi {
namespace iostreams {

/**  Read an entire text file into std::string pointer.
  *  Nullptr when file doesn't exist */
std::shared_ptr<std::string> ReadTextFileIntoString(const ::boost::filesystem::path& filename);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline std::shared_ptr<std::string> ReadTextFileIntoString(const ::boost::filesystem::path& filename) {
    std::shared_ptr<std::string> content;
    std::ifstream file(filename.string());
    if (file) {
        content = std::make_shared<std::string>();
        std::stringstream sstr;
        sstr << file.rdbuf();
        *content = sstr.str();
    }
    return content;
}

}  // namespace iostreams
}  // namespace ferdi