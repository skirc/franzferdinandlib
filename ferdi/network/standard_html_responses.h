#pragma once

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include "mime_type_tags.h"

namespace ferdi {
namespace network {

typedef boost::beast::http::response<boost::beast::http::string_body> BeastStringBodyResponse;

/** Returns a bad request response */
template<class Body, class Allocator>
BeastStringBodyResponse GetBadRequest(const boost::beast::http::request<Body, boost::beast::http::basic_fields<Allocator>>& req,
                                      boost::beast::string_view why);

BeastStringBodyResponse GetNotFound(bool keep_alive, unsigned version, boost::beast::string_view target);
BeastStringBodyResponse GetServerError(bool keep_alive, unsigned version, boost::beast::string_view what);


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
template<class Body, class Allocator>
inline BeastStringBodyResponse GetBadRequest(const boost::beast::http::request<Body, boost::beast::http::basic_fields<Allocator>>& req,
                                             boost::beast::string_view why) {
    BeastStringBodyResponse res{boost::beast::http::status::bad_request, req.version()};
    res.set(boost::beast::http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(boost::beast::http::field::content_type, get_mime_type_text_html());
    res.keep_alive(req.keep_alive());
    res.body() = std::string(why);
    res.prepare_payload();
    return res;
}

/** Returns a not found response */
inline BeastStringBodyResponse GetNotFound(bool keep_alive, unsigned version, boost::beast::string_view target) {
    BeastStringBodyResponse res{boost::beast::http::status::not_found, version };
    res.set(boost::beast::http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(boost::beast::http::field::content_type, get_mime_type_text_html());
    res.keep_alive(keep_alive);
    res.body() = "The resource '" + std::string(target) + "' was not found.";
    res.prepare_payload();
    return res;
}

/** Returns a server error response */
inline BeastStringBodyResponse GetServerError(bool keep_alive, unsigned version, boost::beast::string_view what)  {
    BeastStringBodyResponse res{boost::beast::http::status::internal_server_error, version };
    res.set(boost::beast::http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(boost::beast::http::field::content_type, get_mime_type_text_html());
    res.keep_alive(keep_alive);
    res.body() = "An error occurred: '" + std::string(what) + "'";
    res.prepare_payload();
    return res;
}

} // namespace network
} // namespace ferdi

