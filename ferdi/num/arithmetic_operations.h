// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <functional>
#include <algorithm>
#include <iostream>

namespace ferdi {
namespace num {

// Basic element-wise operations 
template<typename ContainerType> ContainerType Add(const ContainerType& seq1, const ContainerType& seq2);

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
template<typename ContainerType>
inline ContainerType Add(const ContainerType& seq1, const ContainerType& seq2) {  //  Add element-wise
    ContainerType result;
    if (seq1.size() == seq2.size()) {
            result.resize(seq1.size());
            std::transform(seq1.begin(), seq1.end(), seq2.begin(), result.begin(), std::plus<double>());
    } else {
            std::cout << "Add(): input has different shape (" << seq1.size() << " - " << seq2.size() << ")" << std::endl;
    }
    return result;
}

} // namespace num
} // namespace ferdi
