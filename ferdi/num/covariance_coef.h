// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <numeric>

namespace ferdi {
namespace num {

/** Covariance of 2 series. 
  * Return value is the value type of the container. Unlike numpy.cov which returns the matrix  */    
template<typename ContainerType> auto CovCoef(const ContainerType& seq_x, const ContainerType& seq_y);

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
template<typename ContainerType>
inline auto CovCoef(const ContainerType& seq_x, const ContainerType& seq_y) {
    typedef typename ContainerType::value_type ValueType;
    const size_t N = seq_x.size();
    if (N > 1 && !seq_x.empty() &&  N == seq_y.size()) {
        ValueType sum_x(0);
        ValueType sum_y(0);
        ValueType sxy(0);
        auto it_x = seq_x.begin();
        auto it_y = seq_y.begin();
        for (; it_x != seq_x.end(); ++it_x, ++it_y) {
            sum_x += *it_x;
            sum_y += *it_y;
            sxy += (*it_x) * (*it_y);
        }
        const auto avg_x = sum_x / N;
        const auto avg_y = sum_y / N;
        return (sxy - avg_x * avg_y * N) / (N - 1);
    }
    return std::numeric_limits<ValueType>::quiet_NaN();    
}

} // namespace num
} // namespace ferdi