// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#pragma once

#include <vector>

namespace ferdi {
namespace num {

/*****************************************************************************//**
  *  Returns interpolated value at x from parallel arrays ( X-data, Y-data )
  *     - boolean argument extrapolate determines behaviour beyond ends of array (if needed)
  *     - complexity linear  
  *  Assumes that X-data
  *     - has at least two elements
  *     - is sorted and
  *     - is strictly monotonic increasing
  *  TODO:
  *     - Make more robust against nan 
  *     - with the sorted requirement log(N) complexity should be possible 
  *     - templace for other containers                                          */
double Interpolate(const std::vector<double>& x, const std::vector<double>& y, double value, bool extrapolate);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline double Interpolate(const std::vector<double>& x, const std::vector<double>& y, double value, bool extrapolate) {
   const size_t size = x.size();
   if (size < 2) {
       return std::numeric_limits<double>::quiet_NaN();
   }

   int i = 0;                                                            // find left end of interval for interpolation
   if ( value >= x[size - 2] ) {                                         // special case: beyond right end
      i = size - 2;
   }
   else {
      while ( value > x[i+1] ) i++;
   }

   // points on either side (unless beyond ends)
   const double x_left = x[i];
   const double x_right = x[i+1];
   double y_left = y[i];
   double y_right = y[i+1];
   if (!extrapolate) {                                                   // if beyond ends of array and not extrapolating
      if ( value < x_left )  y_right = y_left;
      if ( value > x_right ) y_left  = y_right;
   }

   const double dydx = ( y_right - y_left ) / ( x_right - x_left );      // gradient
   return y_left + dydx * ( value - x_left );                            // linear interpolation
}

} // namespace num
} // namespace ferdi