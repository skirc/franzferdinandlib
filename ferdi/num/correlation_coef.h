// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <cmath>
#include <numeric>

namespace ferdi {
namespace num {

/** Pearson product-moment correlation coefficient. 
  * Return value is the value type of the container. Unlike numpy which returns the matrix  */    
template<typename ContainerType> auto CorrCoef(const ContainerType& seq_x, const ContainerType& seq_y);

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
template<typename ContainerType>
inline auto CorrCoef(const ContainerType& seq_x, const ContainerType& seq_y) {
    typedef typename ContainerType::value_type ValueType;
    const size_t N = seq_x.size();
    if (!seq_x.empty() &&  N == seq_y.size()) {
        const ValueType zero(0);
        ValueType sum_x(0);
        ValueType sum_xx(0);
        ValueType sum_y(0);
        ValueType sum_yy(0);
        ValueType sxy(0);
        auto it_x = seq_x.begin();
        auto it_y = seq_y.begin();
        for (; it_x != seq_x.end(); ++it_x, ++it_y) {
            sum_x += *it_x;
            sum_y += *it_y;
            sum_xx += (*it_x) * (*it_x);
            sum_yy += (*it_y) * (*it_y);
            sxy += (*it_x) * (*it_y);
        }
        const auto avg_x = sum_x / N;
        const auto avg_y = sum_y / N;
        const auto sig_x = sum_xx - avg_x * avg_x * N;
        const auto sig_y = sum_yy - avg_y * avg_y * N;
        if (sig_x > zero && sig_y > zero) {
            const ValueType correlation = sxy - avg_x * avg_y * N;
            return correlation / std::sqrt(sig_x * sig_y);
        }
    }
    return std::numeric_limits<ValueType>::quiet_NaN();
}

} // namespace num
} // namespace ferdi