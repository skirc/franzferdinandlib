// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <vector>
#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <boost/property_tree/ptree.hpp>

namespace ferdi {
namespace python {

template <typename ContainerType>    
ContainerType       ConvertFromPythonList(const boost::python::list& py_list);
std::vector<double> ConvertFromPythonNdarray(const boost::python::numpy::ndarray& py_array);

/** Can handle string, double and dict (recursive) */
boost::property_tree::ptree ConvertFromPythonDictionaryToPtree(const boost::python::dict& dict);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
template <typename ContainerType>    
inline ContainerType ConvertFromPythonList(const boost::python::list& py_list) {
    ContainerType container;
    typedef typename ContainerType::value_type ValueType;
    const auto length = len(py_list);
    for (int i = 0; i < length; ++i) {
        const boost::python::extract<ValueType> extracted_value(py_list[i]);
        if (extracted_value.check()){  
            container.push_back(extracted_value);
        } else {
            throw std::invalid_argument(std::string(__FUNCTION__) + "::Error: invalid value.");
        }
    }
    return container;
}

/** TODO: Find a more elegant way to handle different types */
inline std::vector<double> ConvertFromPythonNdarray(const boost::python::numpy::ndarray& py_array) {
   std::vector<double> cpp_array;
    if (py_array.get_nd() == 1) {
        const auto length = py_array.get_shape()[0];
        const auto dtype = py_array.get_dtype();
        cpp_array.reserve(length);
        if (boost::python::numpy::dtype::get_builtin<double>() == dtype) {
            auto raw_data = reinterpret_cast<double*>(py_array.get_data());
            for (int i=0; i<length; i++) {
                cpp_array.emplace_back(raw_data[i]);
            }
        } else if (boost::python::numpy::dtype::get_builtin<int64_t>() == dtype) {
            auto raw_data = reinterpret_cast<int64_t*>(py_array.get_data());
            for (int i=0; i<length; i++) {
                cpp_array.emplace_back(raw_data[i]);
            }
        } else {
            throw std::invalid_argument(std::string(__FUNCTION__) + " DataType not supported");
        }
    } else {
        throw std::invalid_argument(std::string(__FUNCTION__) + " Can't handle array with " + std::to_string(py_array.get_nd()) + " dimensions");
    }
    return cpp_array;
}

inline boost::property_tree::ptree ConvertFromPythonDictionaryToPtree(const boost::python::dict& dict) {
    boost::property_tree::ptree tree;
    const auto keys = dict.keys();  
    for (int i=0; i<len(keys); ++i) {  
        const boost::python::extract<std::string> extracted_key(keys[i]); 
        const std::string key = extracted_key;  
        
        const boost::python::extract<std::string> extracted_val_str(dict[key]);  
        if (extracted_val_str.check()) {  
            std::string value = extracted_val_str;
            tree.put<std::string>(key, value);
        }  else {
            const boost::python::extract<double> extracted_val_double(dict[key]);  
            if (extracted_val_double.check()) {
                const double value = extracted_val_double;
                tree.put<double>(key, value);
            } else {
                const boost::python::extract<boost::python::dict> extracted_val_dict(dict[key]);
                if (extracted_val_dict.check()) {
                    tree.add_child(key, ConvertFromPythonDictionaryToPtree(extracted_val_dict()));
                } else {
                    throw std::invalid_argument(std::string(__FUNCTION__) + "::Error: don't know how to handle type.");
                }
            }
        }        
    }    
    return tree;
}

}  // namespace python
}  // namespace ferdi
