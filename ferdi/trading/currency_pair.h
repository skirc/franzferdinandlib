// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#pragma once

#include "currency.h"
#include "currency_tools.h"

namespace ferdi {
namespace trd {

/**************************************************************************//**
    * Holds eg EURUSD, USDSGD, as two enums
    * A lot of convenience methods and operators     */
struct CurrencyPair {
    CurrencyPair() {}
    CurrencyPair(Currency base, Currency quote) : base_ccy(base), quote_ccy(quote) {}
    CurrencyPair GetInverted() const;
    Currency     GetOther(Currency ccy) const;
    bool Contains(Currency ccy) const;
    bool ContainsCrypto()  const;
    bool IsTrivial() const { return base_ccy == quote_ccy; }
    bool IsValid()   const; // both ccys are not UNKNOWN

    friend bool operator<(CurrencyPair lhs, CurrencyPair rhs);
    friend bool operator==(CurrencyPair lhs, CurrencyPair rhs);
    friend bool operator!=(CurrencyPair lhs, CurrencyPair rhs);

    Currency base_ccy  = Currency::UNKNOWN;
    Currency quote_ccy = Currency::UNKNOWN;
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline CurrencyPair CurrencyPair::GetInverted() const {
    return CurrencyPair(quote_ccy, base_ccy);
}

inline bool CurrencyPair::Contains(Currency ccy) const {
    return (ccy == base_ccy) || (ccy == quote_ccy);
}

inline bool CurrencyPair::IsValid() const {
    return ferdi::trd::IsValid(base_ccy) && ferdi::trd::IsValid(quote_ccy);
}

inline bool CurrencyPair::ContainsCrypto() const {
    return ferdi::trd::IsCrypto(base_ccy) || ferdi::trd::IsCrypto(quote_ccy);
}

inline Currency CurrencyPair::GetOther(Currency ccy) const {
    if (ccy == base_ccy) {
        return quote_ccy;
    } else if (ccy == quote_ccy) {
        return base_ccy;
    }
    return Currency::UNKNOWN;
}

inline bool operator==(CurrencyPair lhs, CurrencyPair rhs) {
    return (lhs.base_ccy == rhs.base_ccy) && (lhs.quote_ccy == rhs.quote_ccy);
}

inline bool operator!=(CurrencyPair lhs, CurrencyPair rhs) {
    return (lhs.base_ccy != rhs.base_ccy) || (lhs.quote_ccy != rhs.quote_ccy);
}

inline bool operator<(CurrencyPair lhs, CurrencyPair rhs) {
    if (lhs.base_ccy == rhs.base_ccy) {
        return lhs.quote_ccy < rhs.quote_ccy;
    }
    return lhs.base_ccy < rhs.base_ccy;
}

} // namespace trd
} // namespace ferdi
