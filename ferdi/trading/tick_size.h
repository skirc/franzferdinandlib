#pragma once

#include <assert.h>
#include <math.h>
#include "price.h"
#include "trading_enums.h"

namespace ferdi {
namespace trd {

/**************************************************************************//**
	*/
class TickSize : public Price {
public:
    using Price::Price;
    long GetNbrIntTicks(const Price price) const;
    Price GetNearestPrice(const Price price) const;
    Price GetCeilPrice(const ferdi::trd::ExecSide side, const Price price) const;
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline long TickSize::GetNbrIntTicks(const Price price) const {
    assert(this->IsNotZero());
    const double fraction = price.get_value() / this->get_value();
    return std::lrint(fraction);
}

inline Price TickSize::GetNearestPrice(const Price price) const {
    assert(this->IsNotZero());
    const Price fract_ticks = price / this->get_value();
    double integer_part;
    const double fractional_part = modf(fract_ticks.get_value(), &integer_part);
    const Price nearest(integer_part * this->get_value());
    if (fractional_part > 0.5){
        return nearest + *this;
    }
    return nearest;
}

inline Price TickSize::GetCeilPrice(const ferdi::trd::ExecSide side, const Price price) const {
    assert(this->IsNotZero());
    const double fraction = price.get_value() / this->get_value();
    if (side == ferdi::trd::ExecSide::BUY) return Price(this->get_value() * std::ceil(fraction));
    else return Price(this->get_value() * std::floor(fraction));
}

}  // namespace  trd
}  // namespace  ferdi
