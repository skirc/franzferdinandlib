// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#pragma once

#include <cstring>
#include <ferdi/string/string_constants.h>
#include "trading_enums.h"

namespace ferdi {
namespace trd {

////////////////////////////////////////////////////////////////////////////////
const char* ToString(ExecSide side);
const char* ToString(BookSide side);
const char* ToString(PositionSide side);
const char* ToProperCaseString(ExecSide side);
const char* ToProperCaseString(LiquidityType liquidity);
const char* ToProperCaseString(OrderType order_type);
BookSide        BookSideFromString(const char* side);
ExecSide        ExecSideFromString(const char* side);
ExecSide        ExecSideFromChar(char side);
LiquidityType   LiquidityTypeFromString(const char* side);
LiquidityType   LiquidityTypeFromChar(char side);
OrderType       OrderTypeFromString(const char* side);
OrderType       OrderTypeFromChar(char side);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

inline const char* ToString(ExecSide side) {
    switch (side) {
        case ExecSide::BUY:     return "BUY";
        case ExecSide::SELL:    return "SELL";
        case ExecSide::UNKNOWN: return str::StrUnknown();
    }
    return str::StrNotImplemented();
}

inline const char* ToString(BookSide side) {
    switch (side) {
        case BookSide::BID:     return "BID";
        case BookSide::ASK:     return "ASK";
        case BookSide::UNKNOWN: return str::StrUnknown();
    }
    return str::StrNotImplemented();
}


inline const char* ToString(PositionSide side) {
    switch (side) {
        case PositionSide::LONG:    return "LONG";
        case PositionSide::SHORT:   return "SHORT";
        case PositionSide::FLAT:    return "FLAT";
        case PositionSide::UNKNOWN: return str::StrUnknown();
    }
    return str::StrNotImplemented();
}

inline const char* ToProperCaseString(ExecSide side) {
    switch (side) {
        case ExecSide::BUY:     return "Buy";
        case ExecSide::SELL:    return "Sell";
        case ExecSide::UNKNOWN: return str::StrUnknown();
    }
    return str::StrNotImplemented();
}

inline const char* ToProperCaseString(LiquidityType liquidity) {
    switch (liquidity) {
        case LiquidityType::MAKER:            return "Maker";
        case LiquidityType::TAKER:            return "Taker";
        case LiquidityType::ACCOUNTING_OPEN:  return "AccountOpen";
        case LiquidityType::ACCOUNTING_CLOSE: return "AccountClose";
        case LiquidityType::UNKNOWN:          return str::StrUnknown();
    }
    return str::StrNotImplemented();
}

inline const char* ToProperCaseString(OrderType order_type) {
    switch (order_type) {
        case OrderType::Limit:        return "Limit";
        case OrderType::Market:       return "Market";
        case OrderType::PostOnly:     return "PostOnly";
        case OrderType::StopLoss:     return "StopLoss";
        case OrderType::BookTransfer: return "BookTransfer";
        case OrderType::UNKNOWN: return str::StrUnknown();
    }
    return str::StrNotImplemented();
}

inline ExecSide ExecSideFromString(const char* side) {
    if (strcmp("BUY",  side) == 0) return ExecSide::BUY;
    if (strcmp("SELL", side) == 0) return ExecSide::SELL;
    if (strcmp("Buy",  side) == 0) return ExecSide::BUY;
    if (strcmp("Sell", side) == 0) return ExecSide::SELL;
    return ExecSide::UNKNOWN;
}

inline BookSide BookSideFromString(const char* side) {
    if (strcmp("BID",  side) == 0) return BookSide::BID;
    if (strcmp("ASK",  side) == 0) return BookSide::ASK;
    return BookSide::UNKNOWN;
}

inline ExecSide ExecSideFromChar(char side) {
    switch (side) {
        case 'B':   return ExecSide::BUY;
        case 'S':   return ExecSide::SELL;
    }
    return ExecSide::UNKNOWN;
}

inline LiquidityType LiquidityTypeFromString(const char* liquidity_type) {
    if (strcmp("Maker",  liquidity_type) == 0)          return LiquidityType::MAKER;
    if (strcmp("Taker",  liquidity_type) == 0)          return LiquidityType::TAKER;
    if (strcmp("AccountOpen",  liquidity_type) == 0)    return LiquidityType::ACCOUNTING_OPEN;
    if (strcmp("AccountClose",  liquidity_type) == 0)   return LiquidityType::ACCOUNTING_CLOSE;
    return LiquidityType::UNKNOWN;
}

inline LiquidityType LiquidityTypeFromChar(char liquidity_type) {
    switch (liquidity_type) {
        case 'M':   return LiquidityType::MAKER;
        case 'T':   return LiquidityType::TAKER;
        case 'O':   return LiquidityType::ACCOUNTING_OPEN;
        case 'C':   return LiquidityType::ACCOUNTING_CLOSE;
    }
    return LiquidityType::UNKNOWN;
}

inline OrderType OrderTypeFromString(const char* order_type) {
    if (strcmp("Limit",  order_type) == 0)          return OrderType::Limit;
    if (strcmp("Market",  order_type) == 0)         return OrderType::Market;
    if (strcmp("PostOnly",  order_type) == 0)       return OrderType::PostOnly;
    if (strcmp("StopLoss",  order_type) == 0)       return OrderType::StopLoss;
    if (strcmp("BookTransfer",  order_type) == 0)   return OrderType::BookTransfer;
    if (strcmp("L",  order_type) == 0)              return OrderType::Limit;
    if (strcmp("M",  order_type) == 0)              return OrderType::Market;
    if (strcmp("P",  order_type) == 0)              return OrderType::PostOnly;
    if (strcmp("S",  order_type) == 0)              return OrderType::StopLoss;
    if (strcmp("B",  order_type) == 0)              return OrderType::BookTransfer;
    return OrderType::UNKNOWN;
}

inline OrderType OrderTypeFromChar(char order_type) {
    switch (order_type) {
        case 'L' :  return OrderType::Limit;
        case 'M' :  return OrderType::Market;
        case 'P' :  return OrderType::PostOnly;
        case 'S' :  return OrderType::StopLoss;
        case 'B' :  return OrderType::BookTransfer;
    }
    return OrderType::UNKNOWN;
}

}  // namespace trd
}  // namespace ferdi
