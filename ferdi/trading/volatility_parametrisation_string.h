// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#pragma once

#include <cstring>
#include <ferdi/string/string_constants.h>
#include "volatility_parametrisation_enums.h"

namespace ferdi {
namespace trd {

////////////////////////////////////////////////////////////////////////////////
const char* ToString(VolatilityParametrisationType type);

VolatilityParametrisationType VolatilityParametrisationFromString(const char* type);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline const char* ToString(VolatilityParametrisationType type) {
    switch (type) {
        case VolatilityParametrisationType::ATM:     return "ATM";
        case VolatilityParametrisationType::RR10:    return "RR10";
        case VolatilityParametrisationType::RR25:    return "RR25";
        case VolatilityParametrisationType::S10:     return "S10";
        case VolatilityParametrisationType::S25:     return "S25";
        case VolatilityParametrisationType::UNKNOWN: return str::StrUnknown();
    }
    return str::StrNotImplemented();
}

inline VolatilityParametrisationType VolatilityParametrisationFromString(const char* type) {
    if (strcmp("ATM",   type) == 0) return VolatilityParametrisationType::ATM;
    if (strcmp("RR10",  type) == 0) return VolatilityParametrisationType::RR10;
    if (strcmp("RR25",  type) == 0) return VolatilityParametrisationType::RR25;
    if (strcmp("S10",  type) == 0)  return VolatilityParametrisationType::S10;
    if (strcmp("S25",  type) == 0)  return VolatilityParametrisationType::S25;
    return VolatilityParametrisationType::UNKNOWN;
}

}  // namespace trd
}  // namespace ferdi
