// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <iostream>
#include "currency.h"
#include "currency_string.h"

namespace ferdi {
namespace trd {
    
/**************************************************************************//**
    *  A CashFlow is just a simple (amount, currency) pair. */
template <typename AmountType>    
struct CashFlow {
    template <typename A> friend std::ostream& operator<<(std::ostream& os, const CashFlow<A>& cf);
    CashFlow(AmountType a, Currency c) : amount(a), ccy(c) {}
    bool IsValid() const { return ccy != Currency::UNKNOWN; }
    
    bool operator==(const CashFlow& rhs) const;
    bool operator!=(const CashFlow& rhs) const;
    CashFlow operator*(double rhs) const;
    
    AmountType  amount;
    Currency    ccy = Currency::UNKNOWN;
};

////////////////////////////////////////////////////////////////////////////////
template <typename AmountType>
inline bool CashFlow<AmountType>::operator==(const CashFlow<AmountType>& rhs) const {
    return (ccy == rhs.ccy) && (amount == rhs.amount);
}

template <typename AmountType>
inline bool CashFlow<AmountType>::operator!=(const CashFlow<AmountType>& rhs) const {
    return (ccy != rhs.ccy) || (amount != rhs.amount);
}

template <typename AmountType>
inline CashFlow<AmountType> CashFlow<AmountType>::operator*(double rhs) const { 
    return CashFlow(amount * rhs, ccy); 
}

template <typename AmountType>
inline std::ostream& operator<<(std::ostream& os, const CashFlow<AmountType>& cf) { 
    os << cf.amount << " " << ToString(cf.ccy);
    return os; 
}

}  // namespace  trd
}  // namespace  ferdi