// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#pragma once

#include <cstring>
#include <ferdi/string/string_constants.h>
#include "option_style_enums.h"

namespace ferdi {
namespace trd {

////////////////////////////////////////////////////////////////////////////////
const char* ToString(OptionStyle style);
OptionStyle OptionStyleFromString(const char* style);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline const char* ToString(OptionStyle style) {
    switch (style) {
        case OptionStyle::EUROPEAN: return "EUROPEAN";
        case OptionStyle::AMERICAN: return "AMERICAN";
        case OptionStyle::ASIAN:    return "ASIAN";
        case OptionStyle::BERMUDAN: return "BERMUDAN";
        case OptionStyle::CANARY:   return "CANARY";
        case OptionStyle::UNKNOWN:  return str::StrUnknown();
    }
    return str::StrNotImplemented();
}

inline OptionStyle OptionStyleFromString(const char* style) {
    if (strcmp("EUROPEAN", style) == 0) return OptionStyle::EUROPEAN;
    if (strcmp("European", style) == 0) return OptionStyle::EUROPEAN;
    if (strcmp("AMERICAN", style) == 0) return OptionStyle::AMERICAN;
    if (strcmp("American", style) == 0) return OptionStyle::AMERICAN;
    if (strcmp("ASIAN",    style) == 0) return OptionStyle::ASIAN;
    if (strcmp("Asian",    style) == 0) return OptionStyle::ASIAN;
    if (strcmp("BERMUDAN", style) == 0) return OptionStyle::BERMUDAN;
    if (strcmp("Bermudan", style) == 0) return OptionStyle::BERMUDAN;
    if (strcmp("CANARY",   style) == 0) return OptionStyle::CANARY;
    if (strcmp("Canary",   style) == 0) return OptionStyle::CANARY;
    return OptionStyle::UNKNOWN;
}

}  // namespace trd
}  // namespace ferdi
