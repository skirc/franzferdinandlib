// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <map>
#include "cash_flow.h"

namespace ferdi {
namespace trd {

/**************************************************************************//**
    *  Holds N cashflows (with N = 0, 1, 2, .....) 
    *  many operators are defined 
    *  Internally cashflows are kept sorted by currency */
template <typename AmountType>
class CashFlows {
    template <typename A> friend std::ostream& operator<<(std::ostream& os, const CashFlows<A>& cfs);
    typedef std::map<Currency, AmountType>  CashFlowMap;
    typedef typename CashFlowMap::const_iterator const_iterator;
public:
    CashFlows() {}
    CashFlows(CashFlow<AmountType> cf) { cash_flows_.emplace(cf.ccy, cf.amount); }
    
    void Add(const CashFlow<AmountType> cf);
    bool HasCurrency(Currency ccy) const;
    AmountType GetCurrencyAmount(Currency ccy) const; // zero if not exist
    
    // container like methods
    size_t size() const { return cash_flows_.size(); }
    bool empty() const { return cash_flows_.empty(); }
    const_iterator begin() const { return cash_flows_.begin(); }
    const_iterator end()   const { return cash_flows_.end(); }
    
    // user friendly operators
    template <typename A> friend bool operator==(const CashFlows<A>& lhs, const CashFlows<A>& rhs);
    template <typename A> friend bool operator==(const CashFlows<A>& lhs, const CashFlow<A>& rhs);
    template <typename A> friend CashFlows<A> operator+(const CashFlow<A>& lhs,  const CashFlow<A>& rhs);
    template <typename A> friend CashFlows<A> operator-(const CashFlow<A>& lhs,  const CashFlow<A>& rhs);
    template <typename A> friend CashFlows<A> operator+(const CashFlows<A>& lhs, const CashFlow<A>& rhs);
    
private:
    CashFlowMap cash_flows_;
};

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
template <typename AmountType> 
inline bool CashFlows<AmountType>::HasCurrency(Currency ccy) const {
    return (cash_flows_.find(ccy) != cash_flows_.end());
}

template <typename AmountType>
inline AmountType CashFlows<AmountType>::GetCurrencyAmount(Currency ccy) const { // zero if not exist
    auto it = cash_flows_.find(ccy);
    if (it != cash_flows_.end()) {
        return it->second;
    }    
    return AmountType(0);
}

template <typename AmountType>
inline void CashFlows<AmountType>::Add(const CashFlow<AmountType> cf) {
    auto it = cash_flows_.find(cf.ccy);
    if (it != cash_flows_.end()) {
        it->second += cf.amount;
    } else {
        cash_flows_.emplace(cf.ccy, cf.amount);
    }                                    
}

template <typename AmountType>
inline CashFlows<AmountType> operator+(const CashFlow<AmountType>& lhs, const CashFlow<AmountType>& rhs) {
    CashFlows<AmountType> cash_flows;
    if (lhs.ccy == rhs.ccy) {
        cash_flows.cash_flows_.emplace(lhs.ccy, lhs.amount + rhs.amount);
    } else {
        cash_flows.cash_flows_.emplace(lhs.ccy, lhs.amount);
        cash_flows.cash_flows_.emplace(rhs.ccy, rhs.amount);
    }
    return cash_flows;
}

template <typename AmountType> 
inline CashFlows<AmountType> operator-(const CashFlow<AmountType>& lhs, const CashFlow<AmountType>& rhs) {
    CashFlows<AmountType> cash_flows;
    if (lhs.ccy == rhs.ccy) {
        cash_flows.cash_flows_.emplace(lhs.ccy, lhs.amount - rhs.amount);
    } else {
        cash_flows.cash_flows_.emplace(lhs.ccy, lhs.amount);
        cash_flows.cash_flows_.emplace(rhs.ccy, rhs.amount * -1.0);
    }
    return cash_flows;    
}

template <typename AmountType> 
inline CashFlows<AmountType> operator+(const CashFlows<AmountType>& lhs, const CashFlow<AmountType>& rhs) {
    CashFlows<AmountType> cash_flows(lhs);
    cash_flows.Add(rhs);
    return cash_flows;
}


template <typename AmountType> 
inline bool operator==(const CashFlows<AmountType>& lhs, const CashFlows<AmountType>& rhs) {
    return lhs.cash_flows_ == rhs.cash_flows_;
}

template <typename AmountType>
inline bool operator==(const CashFlows<AmountType>& lhs, const CashFlow<AmountType>& rhs) {
    if (lhs.cash_flows_.size() == 1) {
        return lhs.cash_flows_.begin()->second == rhs.amount;
    }
    return false;
}

template <typename AmountType>
inline std::ostream& operator<<(std::ostream& os, const CashFlows<AmountType>& cfs) {
    if (!cfs.empty()) {
        auto it = cfs.cash_flows_.begin();
        os << it->second << " " << ToString(it->first);
        ++it;
        for (; it != cfs.cash_flows_.end(); ++it) {
            os << ", " << it->second << " " << ToString(it->first);
        }
    }
    return os;
}

}  // namespace  trd
}  // namespace  ferdi