// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <string>
#include "currency.h"
#include "currency_pair.h"

namespace ferdi {
namespace trd {

std::string  ToString(Currency ccy);
std::string  ToString(CurrencyPair pair);            // EURUSD
std::string  ToDelimitedString(CurrencyPair pair);   // EUR/USD
Currency     CurrencyFromString(const std::string& ccy);    
CurrencyPair CurrencyPairFromString(const std::string& ccy);   // EURUSD 

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

inline std::string ToString(Currency ccy) {
    switch (ccy) {
        //---------- Major currencies
        case Currency::USD:     return "USD";
        case Currency::EUR:     return "EUR";
        case Currency::JPY:     return "JPY";
        case Currency::GBP:     return "GBP";
        case Currency::CHF:     return "CHF";
        case Currency::AUD:     return "AUD";
        case Currency::NZD:     return "NZD";
        case Currency::CAD:     return "CAD";

        //---------- Major South East Asia
        case Currency::IDR:     return "IDR";
        case Currency::MYR:     return "MYR";
        case Currency::PHP:     return "PHP";
        case Currency::SGD:     return "SGD";
        case Currency::VND:     return "VND";

        //---------- Other currencies
        case Currency::BND:     return "BND";
        case Currency::DKK:     return "DKK";
        case Currency::NOK:     return "NOK";
        case Currency::SEK:     return "SEK";
        case Currency::HKD:     return "HKD";
        case Currency::CZK:     return "CZK";
        case Currency::HUF:     return "HUF";
        case Currency::ISK:     return "ISK";
        case Currency::PLN:     return "PLN";
        case Currency::RON:     return "RON";
        case Currency::AOA:     return "AOA";
        case Currency::AFN:     return "AFN";
        case Currency::AMD:     return "AMD";
        case Currency::ALL:     return "ALL";
        case Currency::AWG:     return "AWG";
        case Currency::ARS:     return "ARS";
        case Currency::AZN:     return "AZN";
        case Currency::DZD:     return "DZD";
        case Currency::BBD:     return "BBD";
        case Currency::BAM:     return "BAM";
        case Currency::BIF:     return "BIF";
        case Currency::BGN:     return "BGN";
        case Currency::BDT:     return "BDT";
        case Currency::BSD:     return "BSD";
        case Currency::BRL:     return "BRL";
        case Currency::BOB:     return "BOB";
        case Currency::BZD:     return "BZD";
        case Currency::BTN:     return "BTN";
        case Currency::BMD:     return "BMD";
        case Currency::BHD:     return "BHD";
        case Currency::KHR:     return "KHR";
        case Currency::KMF:     return "KMF";
        case Currency::KYD:     return "KYD";
        case Currency::CDF:     return "CDF";
        case Currency::CLP:     return "CLP";
        case Currency::COP:     return "COP";
        case Currency::CNY:     return "CNY";
        case Currency::CRC:     return "CRC";
        case Currency::CUP:     return "CUP";
        case Currency::CVE:     return "CVE";
        case Currency::HRK:     return "HRK";
        case Currency::XAF:     return "XAF";
        case Currency::XPF:     return "XPF";
        case Currency::DJF:     return "DJF";
        case Currency::DOP:     return "DOP";
        case Currency::EGP:     return "EGP";
        case Currency::ERN:     return "ERN";
        case Currency::ETB:     return "ETB";
        case Currency::XCD:     return "XCD";
        case Currency::GNF:     return "GNF";
        case Currency::GMD:     return "GMD";
        case Currency::GHS:     return "GHS";
        case Currency::GEL:     return "GEL";
        case Currency::GTQ:     return "GTQ";
        case Currency::GYD:     return "GYD";
        case Currency::HNL:     return "HNL";
        case Currency::HTG:     return "HTG";
        case Currency::IRR:     return "IRR";
        case Currency::IQD:     return "IQD";
        case Currency::INR:     return "INR";
        case Currency::ILS:     return "ILS";
        case Currency::KRW:     return "KRW";
        case Currency::JOD:     return "JOD";
        case Currency::JMD:     return "JMD";
        case Currency::KPW:     return "KPW";
        case Currency::KGS:     return "KGS";
        case Currency::KES:     return "KES";
        case Currency::KWD:     return "KWD";
        case Currency::KZT:     return "KZT";
        case Currency::LBP:     return "LBP";
        case Currency::LAK:     return "LAK";
        case Currency::LRD:     return "LRD";
        case Currency::LSL:     return "LSL";
        case Currency::LYD:     return "LYD";
        case Currency::MDL:     return "MDL";
        case Currency::MAD:     return "MAD";
        case Currency::MGA:     return "MGA";
        case Currency::MRO:     return "MRO";
        case Currency::MNT:     return "MNT";
        case Currency::MMK:     return "MMK";
        case Currency::MKD:     return "MKD";
        case Currency::MWK:     return "MWK";
        case Currency::MUR:     return "MUR";
        case Currency::MXN:     return "MXN";
        case Currency::MVR:     return "MVR";
        case Currency::MOP:     return "MOP";
        case Currency::MZN:     return "MZN";
        case Currency::ANG:     return "ANG";
        case Currency::NAD:     return "NAD";
        case Currency::NIO:     return "NIO";
        case Currency::NGN:     return "NGN";
        case Currency::NPR:     return "NPR";
        case Currency::OMR:     return "OMR";
        case Currency::PEN:     return "PEN";
        case Currency::PKR:     return "PKR";
        case Currency::PAB:     return "PAB";
        case Currency::PYG:     return "PYG";
        case Currency::QAR:     return "QAR";
        case Currency::RWF:     return "RWF";
        case Currency::RUB:     return "RUB";
        case Currency::LKR:     return "LKR";
        case Currency::RSD:     return "RSD";
        case Currency::SAR:     return "SAR";
        case Currency::SCR:     return "SCR";
        case Currency::SOS:     return "SOS";
        case Currency::SLL:     return "SLL";
        case Currency::STD:     return "STD";
        case Currency::SRD:     return "SRD";
        case Currency::SVC:     return "SVC";
        case Currency::SZL:     return "SZL";
        case Currency::SYP:     return "SYP";
        case Currency::ZAR:     return "ZAR";
        case Currency::SDG:     return "SDG";
        case Currency::THB:     return "THB";
        case Currency::TJS:     return "TJS";
        case Currency::TRY:     return "TRY";
        case Currency::TND:     return "TND";
        case Currency::TWD:     return "TWD";
        case Currency::TTD:     return "TTD";
        case Currency::TZS:     return "TZS";
        case Currency::AED:     return "AED";
        case Currency::UAH:     return "UAH";
        case Currency::UGX:     return "UGX";
        case Currency::UZS:     return "UZS";
        case Currency::UYU:     return "UYU";
        case Currency::VEF:     return "VEF";
        case Currency::VUV:     return "VUV";
        case Currency::XOF:     return "XOF";
        case Currency::YER:     return "YER";
        case Currency::ZMW:     return "ZMW";
        case Currency::LTL:     return "LTL";
        case Currency::LVL:     return "LVL";
        case Currency::ZWD:     return "ZWD";
        case Currency::BYR:     return "BYR";
        case Currency::ECS:     return "ECS";
        case Currency::BWP:     return "BWP";
        case Currency::FKP:     return "FKP";
        case Currency::FJD:     return "FJD";
        case Currency::GIP:     return "GIP";
        case Currency::PGK:     return "PGK";
        
        // ------------------- Crypto Currencies
        case Currency::XBT:     return "XBT";
        case Currency::ETH:     return "ETH";
        case Currency::LTC:     return "LTC";
        
        // Other
        case Currency::UNKNOWN: return "Unknown";
    }
    return "NotImplemented";
}

inline std::string ToString(CurrencyPair pair) {
    return ToString(pair.base_ccy) + ToString(pair.quote_ccy);
}

inline std::string ToDelimitedString(CurrencyPair pair) {   // EUR/USD
    return ToString(pair.base_ccy) + '/' + ToString(pair.quote_ccy);
}

inline Currency CurrencyFromString(const std::string& ccy) {
    //---------- Major currencies
    if (ccy == "USD") return Currency::USD;
    if (ccy == "EUR") return Currency::EUR;
    if (ccy == "JPY") return Currency::JPY;
    if (ccy == "GBP") return Currency::GBP;
    if (ccy == "CHF") return Currency::CHF;
    if (ccy == "AUD") return Currency::AUD;
    if (ccy == "NZD") return Currency::NZD;
    if (ccy == "CAD") return Currency::CAD;
    
    //---------- Major South East Asia
    if (ccy == "IDR") return Currency::IDR;
    if (ccy == "MYR") return Currency::MYR;
    if (ccy == "PHP") return Currency::PHP;
    if (ccy == "SGD") return Currency::SGD;
    if (ccy == "VND") return Currency::VND;
    
    //---------- Other currencies
    if (ccy == "BND") return Currency::BND;    
    if (ccy == "DKK") return Currency::DKK;
    if (ccy == "NOK") return Currency::NOK;
    if (ccy == "SEK") return Currency::SEK;
    if (ccy == "HKD") return Currency::HKD;
    if (ccy == "CZK") return Currency::CZK;
    if (ccy == "HUF") return Currency::HUF;
    if (ccy == "ISK") return Currency::ISK;
    if (ccy == "PLN") return Currency::PLN;
    if (ccy == "RON") return Currency::RON;
    if (ccy == "AOA") return Currency::AOA;
    if (ccy == "AFN") return Currency::AFN;
    if (ccy == "AMD") return Currency::AMD;
    if (ccy == "ALL") return Currency::ALL;
    if (ccy == "AWG") return Currency::AWG;
    if (ccy == "ARS") return Currency::ARS;
    if (ccy == "AZN") return Currency::AZN;
    if (ccy == "DZD") return Currency::DZD;
    if (ccy == "BBD") return Currency::BBD;
    if (ccy == "BAM") return Currency::BAM;
    if (ccy == "BIF") return Currency::BIF;
    if (ccy == "BGN") return Currency::BGN;
    if (ccy == "BDT") return Currency::BDT;
    if (ccy == "BSD") return Currency::BSD;
    if (ccy == "BRL") return Currency::BRL;
    if (ccy == "BOB") return Currency::BOB;
    if (ccy == "BZD") return Currency::BZD;
    if (ccy == "BTN") return Currency::BTN;
    if (ccy == "BMD") return Currency::BMD;
    if (ccy == "BHD") return Currency::BHD;
    if (ccy == "KHR") return Currency::KHR;
    if (ccy == "KMF") return Currency::KMF;
    if (ccy == "KYD") return Currency::KYD;
    if (ccy == "CDF") return Currency::CDF;
    if (ccy == "CLP") return Currency::CLP;
    if (ccy == "COP") return Currency::COP;
    if (ccy == "CNY") return Currency::CNY;
    if (ccy == "CRC") return Currency::CRC;
    if (ccy == "CUP") return Currency::CUP;
    if (ccy == "CVE") return Currency::CVE;
    if (ccy == "HRK") return Currency::HRK;
    if (ccy == "XAF") return Currency::XAF;
    if (ccy == "XPF") return Currency::XPF;
    if (ccy == "DJF") return Currency::DJF;
    if (ccy == "DOP") return Currency::DOP;
    if (ccy == "EGP") return Currency::EGP;
    if (ccy == "ERN") return Currency::ERN;
    if (ccy == "ETB") return Currency::ETB;
    if (ccy == "XCD") return Currency::XCD;
    if (ccy == "GNF") return Currency::GNF;
    if (ccy == "GMD") return Currency::GMD;
    if (ccy == "GHS") return Currency::GHS;
    if (ccy == "GEL") return Currency::GEL;
    if (ccy == "GTQ") return Currency::GTQ;
    if (ccy == "GYD") return Currency::GYD;
    if (ccy == "HNL") return Currency::HNL;
    if (ccy == "HTG") return Currency::HTG;
    if (ccy == "IRR") return Currency::IRR;
    if (ccy == "IQD") return Currency::IQD;
    if (ccy == "INR") return Currency::INR;
    if (ccy == "ILS") return Currency::ILS;
    if (ccy == "KRW") return Currency::KRW;
    if (ccy == "JOD") return Currency::JOD;
    if (ccy == "JMD") return Currency::JMD;
    if (ccy == "KPW") return Currency::KPW;
    if (ccy == "KGS") return Currency::KGS;
    if (ccy == "KES") return Currency::KES;
    if (ccy == "KWD") return Currency::KWD;
    if (ccy == "KZT") return Currency::KZT;
    if (ccy == "LBP") return Currency::LBP;
    if (ccy == "LAK") return Currency::LAK;
    if (ccy == "LRD") return Currency::LRD;
    if (ccy == "LSL") return Currency::LSL;
    if (ccy == "LYD") return Currency::LYD;
    if (ccy == "MDL") return Currency::MDL;
    if (ccy == "MAD") return Currency::MAD;
    if (ccy == "MGA") return Currency::MGA;
    if (ccy == "MRO") return Currency::MRO;
    if (ccy == "MNT") return Currency::MNT;
    if (ccy == "MMK") return Currency::MMK;
    if (ccy == "MKD") return Currency::MKD;
    if (ccy == "MWK") return Currency::MWK;
    if (ccy == "MUR") return Currency::MUR;
    if (ccy == "MXN") return Currency::MXN;
    if (ccy == "MVR") return Currency::MVR;
    if (ccy == "MOP") return Currency::MOP;
    if (ccy == "MZN") return Currency::MZN;
    if (ccy == "ANG") return Currency::ANG;
    if (ccy == "NAD") return Currency::NAD;
    if (ccy == "NIO") return Currency::NIO;
    if (ccy == "NGN") return Currency::NGN;
    if (ccy == "NPR") return Currency::NPR;
    if (ccy == "OMR") return Currency::OMR;
    if (ccy == "PEN") return Currency::PEN;
    if (ccy == "PKR") return Currency::PKR;
    if (ccy == "PAB") return Currency::PAB;
    if (ccy == "PYG") return Currency::PYG;
    if (ccy == "QAR") return Currency::QAR;
    if (ccy == "RWF") return Currency::RWF;
    if (ccy == "RUB") return Currency::RUB;
    if (ccy == "LKR") return Currency::LKR;
    if (ccy == "RSD") return Currency::RSD;
    if (ccy == "SAR") return Currency::SAR;
    if (ccy == "SCR") return Currency::SCR;
    if (ccy == "SOS") return Currency::SOS;
    if (ccy == "SLL") return Currency::SLL;
    if (ccy == "STD") return Currency::STD;
    if (ccy == "SRD") return Currency::SRD;
    if (ccy == "SVC") return Currency::SVC;
    if (ccy == "SZL") return Currency::SZL;
    if (ccy == "SYP") return Currency::SYP;
    if (ccy == "ZAR") return Currency::ZAR;
    if (ccy == "SDG") return Currency::SDG;
    if (ccy == "THB") return Currency::THB;
    if (ccy == "TJS") return Currency::TJS;
    if (ccy == "TRY") return Currency::TRY;
    if (ccy == "TND") return Currency::TND;
    if (ccy == "TWD") return Currency::TWD;
    if (ccy == "TTD") return Currency::TTD;
    if (ccy == "TZS") return Currency::TZS;
    if (ccy == "AED") return Currency::AED;
    if (ccy == "UAH") return Currency::UAH;
    if (ccy == "UGX") return Currency::UGX;
    if (ccy == "UZS") return Currency::UZS;
    if (ccy == "UYU") return Currency::UYU;
    if (ccy == "VEF") return Currency::VEF;
    if (ccy == "VUV") return Currency::VUV;
    if (ccy == "XOF") return Currency::XOF;
    if (ccy == "YER") return Currency::YER;
    if (ccy == "ZMW") return Currency::ZMW;
    if (ccy == "LTL") return Currency::LTL;
    if (ccy == "LVL") return Currency::LVL;
    if (ccy == "ZWD") return Currency::ZWD;
    if (ccy == "BYR") return Currency::BYR;
    if (ccy == "ECS") return Currency::ECS;
    if (ccy == "BWP") return Currency::BWP;
    if (ccy == "FKP") return Currency::FKP;
    if (ccy == "FJD") return Currency::FJD;
    if (ccy == "GIP") return Currency::GIP;
    if (ccy == "PGK") return Currency::PGK;
    
    // ------------------- Crypto Currencies
    if (ccy == "XBT") return Currency::XBT;
    if (ccy == "ETH") return Currency::ETH;
    if (ccy == "LTC") return Currency::LTC;
    return Currency::UNKNOWN;
}

inline CurrencyPair CurrencyPairFromString(const std::string& ccy) {
    if (ccy.size() == 6) {
        return CurrencyPair(CurrencyFromString(ccy.substr(0,3)), CurrencyFromString(ccy.substr(3)));
    } else {
        return CurrencyPair(Currency::UNKNOWN, Currency::UNKNOWN);
    }
}

} // namespace trd
} // namespace ferdi

