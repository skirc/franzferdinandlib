// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

namespace ferdi {
namespace trd {

///////////////////////////////////////////////////////////////////////////////
enum class Currency : unsigned char {
    
    // ------------- Fiat Currencies
    USD, EUR, JPY, GBP, AUD, CAD, CHF, NZD,  // Mayors
    
    // Europe
    NOK, SEK, HUF, PLN, CZK, RUB, DKK, ISK, RON, ALL, 
    BAM, BGN, HRK, GEL, MDL, MKD, UAH, LTL, LVL, BYR, 
    
    // East Asia
    SGD, HKD, KRW, THB, MYR, PHP, TWD, IDR, BND, KHR,
    CNY, KPW, LAK, MNT, MMK, MOP, VND, 
    
    // Remaining Asia
    INR, ILS, TRY, AFN, AMD, AZN, BDT, BTN, BMD, BHD,
    IRR, IQD, JOD, KZT, LBP, NPR, OMR, PKR, QAR, LKR, 
    RSD, SAR, SYP, TJS, AED, UZS, YER, PGK, KGS, KWD,
    
    // Africa
    ZAR, AOA, DZD, KMF, CDF, CVE, XAF, XPF, DJF, EGP,
    ERN, ETB, XCD, GNF, GMD, GHS, LRD, LSL, LYD, MAD,
    MWK, MUR, MVR, MZN, ANG, NAD, NGN, RWF, SCR, SOS, 
    SLL, STD, SRD, SZL, SDG, TND, TTD, TZS, UGX, VUV, 
    XOF, ZMW, ZWD, BWP, GIP, GYD, MGA, KES,
        
    // America    
    BRL, MXN, CLP, ARS, AWG, BBD, BIF, BSD, BOB, BZD, 
    KYD, COP, CRC, CUP, DOP, GTQ, HNL, HTG, JMD, MRO,
    NIO, PEN, PAB, PYG, SVC, UYU, VEF, ECS, FKP, FJD,
    
    // ------------- Crypto Currencies
    XBT, 
    ETH,
    LTC,   // Litecoin
    // ------------- Other
    UNKNOWN
};

} // namespace trd
} // namespace ferdi
