// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#pragma once

#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>

namespace ferdi {
namespace tec {

/**  Get unique temporary filename with a prefix and a postfix like
  *  eg '/tmp/unit_test_fdj34fsd.xml'  */
boost::filesystem::path GetUniqueTmpFilename(const std::string& prefix, const std::string& suffix);


/**************************************************************************//**
   *  File will be removed when class goes out of scope.
   *  Useful for unit tests */
class AutoDeletingTmpFilename {
public:
    AutoDeletingTmpFilename(const std::string& prefix, const std::string& suffix);
    ~AutoDeletingTmpFilename();
    const ::boost::filesystem::path& get_filename() const { return tmp_filename_; }

private:
    const ::boost::filesystem::path tmp_filename_;
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline boost::filesystem::path GetUniqueTmpFilename(const std::string& prefix, const std::string& suffix) {
    return boost::filesystem::temp_directory_path() /
           (prefix + boost::filesystem::unique_path().string() + suffix);
}

inline AutoDeletingTmpFilename::AutoDeletingTmpFilename(const std::string& prefix, const std::string& suffix)
    : tmp_filename_(GetUniqueTmpFilename(prefix, suffix)) {}

inline AutoDeletingTmpFilename::~AutoDeletingTmpFilename() {
    if (exists(tmp_filename_)) {
        boost::filesystem::remove(tmp_filename_);
    }
}

}  // namespace tec
}  // namespace ferdi



