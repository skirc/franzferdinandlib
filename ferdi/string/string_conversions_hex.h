// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <string>
#include "string_constants.h"

namespace ferdi {
namespace tec {
  
std::string StringToHex(const std::string& s);
std::string HexToString(const std::string& s);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline std::string StringToHex(const std::string& s) {
    const size_t len = s.length();
    std::string output;
    output.reserve(2 * len);
    for (size_t i = 0; i < len; ++i) {
        const unsigned char c = s[i];
        output.push_back(str::GetOrderedHexadecimalDigits()[c >> 4]);
        output.push_back(str::GetOrderedHexadecimalDigits()[c & 15]);
    }
    return output;
}

inline std::string HexToString(const std::string& s) {
    const size_t len = s.length();
    if (len == 0) return std::string();
    if (len & 1) throw std::invalid_argument("odd length");

    std::string output;
    output.reserve(len / 2);
    for (size_t i = 0; i < len; i += 2) {
        const char a = s[i];
        const char* p = std::lower_bound(str::GetOrderedHexadecimalDigits(), str::GetOrderedHexadecimalDigits() + 16, a);
        if (*p != a) throw std::invalid_argument("not a hex digit");

        const char b = s[i + 1];
        const char* q = std::lower_bound(str::GetOrderedHexadecimalDigits(), str::GetOrderedHexadecimalDigits() + 16, b);
        if (*q != b) throw std::invalid_argument("not a hex digit");

        output.push_back(((p - str::GetOrderedHexadecimalDigits()) << 4) | (q - str::GetOrderedHexadecimalDigits()));
    }
    return output;
}

}  // namespace tec
}  // namespace ferdi



